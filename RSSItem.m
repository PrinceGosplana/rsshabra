//
//  RSSItem.m
//  RSSHabrahabr
//
//  Created by Александр Исаев on 17.06.15.
//  Copyright (c) 2015 Oleksandr Isaiev. All rights reserved.
//

#import "RSSItem.h"


@implementation RSSItem

@dynamic dateString;
@dynamic link;
@dynamic mainText;
@dynamic title;
@dynamic dateForSorting;

@end
