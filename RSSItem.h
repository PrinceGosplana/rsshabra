//
//  RSSItem.h
//  RSSHabrahabr
//
//  Created by Александр Исаев on 17.06.15.
//  Copyright (c) 2015 Oleksandr Isaiev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RSSItem : NSManagedObject

@property (nonatomic, retain) NSString * dateString;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSString * mainText;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * dateForSorting;

@end
