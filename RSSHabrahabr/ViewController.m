//
//  ViewController.m
//  RSSHabrahabr
//
//  Created by Oleksandr Isaiev on 16.06.15.
//  Copyright (c) 2015 Oleksandr Isaiev. All rights reserved.
//

#import "ViewController.h"
#import "CustomTableViewCell.h"
#import <SVProgressHUD.h>
#import <MagicalRecord.h>
#import "RSSItem.h"
#import "Reachability.h"
#import "WebViewController.h"

#define DATE_FORMAT @"EEE, dd MM yyyy HH:mm:ss z"

@interface ViewController () {
    
//    NSMutableArray *feeds;
//  
//    NSMutableDictionary *item;
//    NSMutableString *title;
//    NSMutableString *link;
//    NSMutableString * dateString;
//    NSMutableString * date;
//    NSString *element;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  self.navigationController.hidesBarsOnSwipe = YES;
  self.feeds = [NSMutableArray array];
  self.itemsRSS = [NSMutableArray arrayWithCapacity:10];
  
  // create DB if need
  [self fetchAllRSSItems];

}

- (void)viewWillAppear:(BOOL)animated {
    [self animateTable];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Life cycle

- (void) startLoadData {
  
  [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
  [SVProgressHUD show];
  [self removeAllDataFromArray:self.itemsRSS];
  
  NSURL *url = [NSURL URLWithString:@"http://habrahabr.ru/rss/hubs/"];
  self.parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
  [self.parser setDelegate:self];
  [self.parser setShouldResolveExternalEntities:NO];

  [self.parser parse];
}

- (void) removeAllDataFromArray: (NSMutableArray *) array {

  for (RSSItem * itemToRemove in array) {
    if ([itemToRemove isKindOfClass:[RSSItem class]]) {
      [itemToRemove MR_deleteEntity];
      [self saveContext];
      
    }
  }
  [array removeAllObjects];
}

#pragma mark - DB methods

- (void)saveContext {
  // Save ManagedObjectContext using MagicalRecord
  [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}
                            
- (void) fetchAllRSSItems {

  // first try get data from local DB
  NSMutableArray * fetchedItems = [[RSSItem MR_findAllSortedBy:@"dateForSorting" ascending:NO] mutableCopy];
  
  if (fetchedItems.count) {
    [self removeAllDataFromArray:self.itemsRSS];
    self.itemsRSS = [fetchedItems mutableCopy];
  } else {
    self.itemsRSS = [fetchedItems mutableCopy];
  }
  
  
  // try get data from inet
  // if not connected
  if (![self isConnected]){
    // if have local data
    if (self.itemsRSS.count) {
      [self alertWithTitle:@"Warning" andMessage:@"Lost internet connection! You are using the stored data"];
    } else {
      // local data is empty
      [self alertWithTitle:@"Warning" andMessage:@"Lost internet connection!"];
    }
  } else if (!fetchedItems.count && [self isConnected]){
    [self startLoadData];
  }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsRSS.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

  RSSItem * itemRss = self.itemsRSS[indexPath.row];
  
  if (itemRss != nil) {
    [cell configureCellAtIndex:itemRss];
  }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 94;
}


#pragma mark - NSXMLParserDelegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    self.element = elementName;
    if ([self.element isEqualToString:@"item"]) {
        self.item        = [NSMutableDictionary dictionary];
        self.titleCell       = [NSMutableString string];
        self.link        = [NSMutableString string];
        self.dateString  = [NSMutableString string];
        self.date        = [NSMutableString string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"item"]) {
      NSString * titleItem = [self trimCharachters:self.titleCell];
      NSString * linkItem = [self trimCharachters:self.link];

      RSSItem * itemRSS = [RSSItem MR_createEntity];
      itemRSS.title = titleItem;
      itemRSS.link = linkItem;
      itemRSS.dateString = self.dateString;
      itemRSS.dateForSorting = self.date;
      [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([self.element isEqualToString:@"title"]) {
        [self.titleCell appendString:string];
    } else if ([self.element isEqualToString:@"link"]) {
        [self.link appendString:string];
    } else if ([self.element isEqualToString:@"pubDate"]) {
      [self.date appendString:string];
      string = [self trimCharachters:string];
      
      if (string.length) {
        NSString * pubDate = [self getDateItem:string];
          [self.dateString appendString:pubDate];
      }
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
  [self fetchAllRSSItems];
  [self.tableView reloadData];
  [SVProgressHUD dismiss];
}

#pragma mark - Date formatter 

- (NSString *) trimCharachters: (NSString *) string {
  string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
  string = [string stringByReplacingOccurrencesOfString:@"\t" withString:@""];
  return string;
}

- (NSString *) getDateItem: (NSString *) dateStr {
  
  NSDateFormatter *dateFormatterStr = [NSDateFormatter new];
  [dateFormatterStr setTimeZone:[NSTimeZone localTimeZone]];
  
  [dateFormatterStr setDateFormat:DATE_FORMAT];
  NSDate *dateFromStr = [dateFormatterStr dateFromString:dateStr];
  NSDate *currentDate = [NSDate date];
  if (dateFromStr != nil) {
    NSDateComponents *componentsForStr = [[NSCalendar currentCalendar] components: NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute fromDate:dateFromStr];
    NSDateComponents *componentsCurrent = [[NSCalendar currentCalendar] components: NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute fromDate:currentDate];
    
    
    NSInteger diferenceDay = [componentsCurrent day] - [componentsForStr day];
    NSString * str = [self diferenceDay:diferenceDay];
    NSString * retutningFormatterStr = [self stringFormatter:str month:[componentsForStr month] day:[componentsForStr day] countHour:[componentsForStr hour] countMinut:[componentsForStr minute]];
    
    return retutningFormatterStr;
  }
  return @"";
}

- (NSString *) diferenceDay:(NSInteger) countDayBetween {
    switch (countDayBetween) {
        case 0:
            return @"Cегодня";
        case 1:
            return @"Вчера";
        default:
            return @"";
    }
}

- (NSString *) stringFormatter:(NSString *) string month:(NSInteger) month day:(NSInteger) day countHour: (NSInteger) hour countMinut: (NSInteger) min {
  NSString * returningStr;
  NSString * minute = min < 10? [NSString stringWithFormat:@"0%li", (long)min] : [NSString stringWithFormat:@"%li", (long)min];

  if (string.length) {
    returningStr = [NSString stringWithFormat:@"%@ в %li:%@", string, (long)hour, minute];
  } else {
    returningStr = [NSString stringWithFormat:@"%li.%li в %li:%@", (long)month, (long)day, (long)hour, minute];
  }
  return returningStr;
}

#pragma mark - SVProgressHUD

- (void)dismissSuccess {
  [SVProgressHUD showSuccessWithStatus:@"Great Success!"];
}

- (void)dismissError {
  [SVProgressHUD showErrorWithStatus:@"Failed with Error"];
}

#pragma mark - Action

- (IBAction)reloadItemsPressed:(UIBarButtonItem *)sender {
  if ([self isConnected]) {
        [self animateConstraint];
        [self startLoadData];
        [self.tableView reloadData];
  } else {
        [self alertWithTitle:@"Warning" andMessage:@"Lost internet connection!"];
  }
}

#pragma mark - Animation

- (void) animateConstraint {
    self.constraintTopTableView.constant = 180;
    [UIView animateWithDuration:0.8 animations:^{
        [self.view layoutIfNeeded];
    }completion:^(BOOL finished) {
        self.constraintTopTableView.constant = 0;
        [UIView animateWithDuration:0.3 delay:0.0 usingSpringWithDamping:0.2 initialSpringVelocity:15 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
        }];
    }];
}

- (void) animateTable {
    [self.tableView reloadData];
     NSArray * cells = self.tableView.visibleCells;
    float tableHeight = self.tableView.bounds.size.height;
    
    for (CustomTableViewCell * i in cells) {
        CustomTableViewCell * cell = i;
        cell.transform = CGAffineTransformMakeTranslation(0, tableHeight);
    }
    
    int index = 0;
    for (CustomTableViewCell * a in cells) {
        CustomTableViewCell * cell = a;
        [UIView animateWithDuration:1.5 delay:0.05 * index usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionCurveLinear animations:^{
            cell.transform = CGAffineTransformMakeTranslation(0, 0);
        } completion:nil];
        index += 1;
    }
}

#pragma mark - Reachability
- (BOOL) isConnected {
  self.internetConnectionReach = [Reachability reachabilityForInternetConnection];
  
  if([self.internetConnectionReach isReachable]) {
    return YES;
  }
  return NO;
}

#pragma mark - UIAlertView
- (void) alertWithTitle: (NSString *) titleMessage andMessage: (NSString *) message {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:titleMessage message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
    [alert show];
}

#pragma mark - PrepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        RSSItem * itemForURL = [self.itemsRSS objectAtIndex:indexPath.row];
        NSString * stringUrl = itemForURL.link;
        [[segue destinationViewController] setStringURL:stringUrl];
    }
}
@end
