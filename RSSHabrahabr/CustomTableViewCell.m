//
//  CustomTableViewCell.m
//  RSSHabrahabr
//
//  Created by Oleksandr Isaiev on 16.06.15.
//  Copyright (c) 2015 Oleksandr Isaiev. All rights reserved.
//

#import "CustomTableViewCell.h"
#import "RSSItem.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCellAtIndex:(RSSItem *) item {
  self.titleLabel.text = item.title;
  self.dateLabel.text = item.dateString;
}

@end
