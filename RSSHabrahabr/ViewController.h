//
//  ViewController.h
//  RSSHabrahabr
//
//  Created by Oleksandr Isaiev on 16.06.15.
//  Copyright (c) 2015 Oleksandr Isaiev. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RSSItem;
@class Reachability;

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopTableView;

//@property (weak, nonatomic) IBOutlet UINavigationItem *navController;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RSSItem * rssItem;
@property (strong, nonatomic) NSMutableArray * itemsRSS;
@property(strong) Reachability * internetConnectionReach;
@property (strong, nonatomic) NSXMLParser *parser;

@property (strong, nonatomic) NSMutableArray *feeds;

@property (strong, nonatomic) NSMutableDictionary *item;
@property (strong, nonatomic) NSMutableString *titleCell;
@property (strong, nonatomic) NSMutableString *link;
@property (strong, nonatomic) NSMutableString * dateString;
@property (strong, nonatomic) NSMutableString * date;
@property (strong, nonatomic) NSString *element;

- (IBAction)reloadItemsPressed:(UIBarButtonItem *)sender;

@end

