//
//  WebViewController.h
//  RSSHabrahabr
//
//  Created by Oleksandr Isaiev on 17.06.15.
//  Copyright (c) 2015 Oleksandr Isaiev. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Reachability;

@interface WebViewController : UIViewController <UIWebViewDelegate>

@property (copy, nonatomic) NSString * stringURL;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTop;

@property (nonatomic) BOOL startAnimation;
@property(strong) Reachability * internetConnectionReach;

@end
