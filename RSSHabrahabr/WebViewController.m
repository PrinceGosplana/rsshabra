//
//  WebViewController.m
//  RSSHabrahabr
//
//  Created by Oleksandr Isaiev on 17.06.15.
//  Copyright (c) 2015 Oleksandr Isaiev. All rights reserved.
//

#import "WebViewController.h"
#import <SVProgressHUD.h>
#import "Reachability.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.startAnimation = YES;
    
    self.navigationController.hidesBarsOnSwipe = NO;
    

    
    float angle = M_PI_4;
    self.webView.alpha = 0.0;
    self.webView.transform = CGAffineTransformMakeRotation(angle);
    
    if ([self isConnected]) {
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        [SVProgressHUD show];
        
        NSURL *myURL = [NSURL URLWithString: [self.stringURL stringByAddingPercentEscapesUsingEncoding:
                                              NSUTF8StringEncoding]];
        NSURLRequest *request = [NSURLRequest requestWithURL:myURL];
        [self.webView loadRequest:request];
    } else {
        [self alertWithTitle:@"Warning" andMessage:@"Lost internet connection!"];
    }
}

#pragma mark - WebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    if (self.startAnimation) {
        self.startAnimation = NO;
        [SVProgressHUD dismiss];
        [self showWebView];
        
        [UIView animateWithDuration:0.8 delay:0.0 usingSpringWithDamping:0.6 initialSpringVelocity:15 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.webView.alpha = 1.0;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [self resizeWebView];
            
            [UIView animateWithDuration:0.9 delay:0.0 usingSpringWithDamping:0.3 initialSpringVelocity:10 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                [self.view layoutIfNeeded];
                self.webView.alpha = 1.0;
                self.webView.transform = CGAffineTransformMakeRotation(0);
            } completion:^(BOOL finished) {
                self.webView.transform = CGAffineTransformIdentity;
            }];
        }];
    }
}

#pragma mark - SVProgressHUD

- (void)dismissSuccess {
    [SVProgressHUD showSuccessWithStatus:@"Great Success!"];
}

- (void)dismissError {
    [SVProgressHUD showErrorWithStatus:@"Failed with Error"];
}

#pragma mark - Animate constraints

- (void) hideWebView {
    self.constraintTop.constant = - self.view.bounds.size.height;
    self.constraintBottom.constant = self.view.bounds.size.height;
    self.constraintLeading.constant = self.view.bounds.size.width / 4;
    self.constraintTrailing.constant = self.view.bounds.size.width / 4;
}

- (void) showWebView {
    self.constraintTop.constant = self.view.bounds.size.height / 8;
    self.constraintBottom.constant =self.view.bounds.size.height / 8;
    self.constraintLeading.constant = self.view.bounds.size.width / 4;
    self.constraintTrailing.constant = self.view.bounds.size.width / 4;
}
- (void) resizeWebView {
    self.constraintTop.constant = -64; // height navigationBar
    self.constraintBottom.constant = 0;
    self.constraintLeading.constant = 0;
    self.constraintTrailing.constant = 0;
}

#pragma mark - Reachability

- (BOOL) isConnected {
    self.internetConnectionReach = [Reachability reachabilityForInternetConnection];
    
    if([self.internetConnectionReach isReachable]) {
        return YES;
    }
    return NO;
}

#pragma mark - UIAlertView

- (void) alertWithTitle: (NSString *) titleMessage andMessage: (NSString *) message {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:titleMessage message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
    [alert show];
}

@end
