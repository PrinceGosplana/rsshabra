//
//  CustomTableViewCell.h
//  RSSHabrahabr
//
//  Created by Oleksandr Isaiev on 16.06.15.
//  Copyright (c) 2015 Oleksandr Isaiev. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RSSItem;

@interface CustomTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

- (void) configureCellAtIndex:(RSSItem *) item;

@end
